package ru.nsu.fit.popov.springnsu2.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "way")
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class Way {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "user")
    @Length(min = 1, max = 100)
    private String user;

    @Column(name = "_timestamp")
    private Timestamp timestamp;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "way_tag",
            joinColumns =
                    @JoinColumn(name = "way_id", referencedColumnName = "id"),
            inverseJoinColumns = {
                    @JoinColumn(name = "tag_k", referencedColumnName = "k"),
                    @JoinColumn(name = "tag_v", referencedColumnName = "v")
            },
            uniqueConstraints =
                    @UniqueConstraint(columnNames = { "way_id", "tag_k", "tag_v" }))
    private Set<Tag> tags;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "way_node",
            joinColumns =
                    @JoinColumn(name = "way_id", referencedColumnName = "id"),
            inverseJoinColumns =
                    @JoinColumn(name = "node_id", referencedColumnName = "id"),
            uniqueConstraints =
                    @UniqueConstraint(columnNames = { "way_id", "node_id" }))
    private Set<Node> nodes;
}
