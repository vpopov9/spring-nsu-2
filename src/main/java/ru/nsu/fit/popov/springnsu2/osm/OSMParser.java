package ru.nsu.fit.popov.springnsu2.osm;

import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface OSMParser {

    @Value
    @EqualsAndHashCode
    class Entry {
        public static final Entry ROOT = new Entry("", new String[0], null);

        final String name;
        final String[] attributes;
        final Entry parent;
    }

    List<Map.Entry<String, Integer>> countUpdates(InputStream osm) throws XMLStreamException;

    List<Map.Entry<String, Integer>> countKeys(InputStream osm) throws XMLStreamException;

    Collection<Entry> buildSchema(InputStream osm) throws XMLStreamException;

    void loadToDB(InputStream osm) throws XMLStreamException, JAXBException;
}
