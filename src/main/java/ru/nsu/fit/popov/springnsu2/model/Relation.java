package ru.nsu.fit.popov.springnsu2.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "relation")
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(of = "id")
public class Relation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "user")
    @Length(min = 1, max = 100)
    private String user;

    @Column(name = "_timestamp")
    private Timestamp timestamp;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "relation_tag",
            joinColumns =
                    @JoinColumn(name = "relation_id", referencedColumnName = "id",
                            nullable = false),
            inverseJoinColumns = {
                    @JoinColumn(name = "tag_k", referencedColumnName = "k", nullable = false),
                    @JoinColumn(name = "tag_v", referencedColumnName = "v", nullable = false)
            },
            uniqueConstraints =
                    @UniqueConstraint(columnNames = { "relation_id", "tag_k", "tag_v" }))
    private Set<Tag> tags;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "relation_member",
            joinColumns =
                    @JoinColumn(name = "relation_id", referencedColumnName = "id",
                            nullable = false),
            inverseJoinColumns = {
                    @JoinColumn(name = "member_type", referencedColumnName = "type",
                            nullable = false),
                    @JoinColumn(name = "member_ref", referencedColumnName = "ref",
                            nullable = false)
            },
            uniqueConstraints =
                    @UniqueConstraint(columnNames = { "relation_id", "member_type",
                            "member_ref" }))
    private Set<Member> members;
}
