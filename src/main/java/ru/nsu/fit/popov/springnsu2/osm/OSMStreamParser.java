package ru.nsu.fit.popov.springnsu2.osm;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.nsu.fit.popov.springnsu2.model.osm.OsmNode;
import ru.nsu.fit.popov.springnsu2.model.osm.OsmRoot;
import ru.nsu.fit.popov.springnsu2.model.osm.OsmTag;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@Slf4j
public class OSMStreamParser implements OSMParser {

    private interface XMLFunction<T, R> {
        R apply(T t) throws XMLStreamException;
    }

    private interface XMLProducer<R> {
        R apply() throws XMLStreamException;
    }

    @EqualsAndHashCode(of = "uuid")
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    private final static class User {
        private String uuid;
        private String user;
    }

    private static <T, R> R wrapXML(final XMLFunction<T, R> xmlFunction, T t)
            throws XMLStreamException {
        try {
            return xmlFunction.apply(t);
        } catch (XMLStreamException e) {
            log.error("error in XML processing");
            throw e;
        }
    }

    private static <R> R wrapXML(final XMLProducer<R> xmlProducer) throws XMLStreamException {
        try {
            return xmlProducer.apply();
        } catch (XMLStreamException e) {
            log.error("error in XML processing");
            throw e;
        }
    }

    @Autowired
    private JdbcTemplate jdbc;

    private final XMLInputFactory factory = XMLInputFactory.newInstance();

    @Override
    public List<Map.Entry<String, Integer>> countUpdates(final InputStream osm)
            throws XMLStreamException {
        final Map<User, Integer> userUpdates = new HashMap<>();

        final XMLStreamReader xmlReader = osmToXML(osm);
        while (wrapXML(xmlReader::hasNext)) {
            if (wrapXML(xmlReader::next) != XMLStreamConstants.START_ELEMENT ||
                    !"node".equals(xmlReader.getLocalName())) {
                continue;
            }

            final User user = new User();
            for (int i = 0; i < xmlReader.getAttributeCount(); i++) {
                if (xmlReader.getAttributeLocalName(i).equals("user"))
                    user.user = xmlReader.getAttributeValue(i);
                if (xmlReader.getAttributeLocalName(i).equals("uid"))
                    user.uuid = xmlReader.getAttributeValue(i);
            }
            userUpdates.merge(user, 1, (a,b)->a+b );
        }

        return userUpdates.entrySet().stream()
                .map(entry -> Map.entry(entry.getKey().user, entry.getValue()))
                .sorted(Comparator.comparingInt(Map.Entry::getValue))
                .collect(Collectors.toList());
    }

    @Override
    public List<Map.Entry<String, Integer>> countKeys(final InputStream osm) throws XMLStreamException {
        final Map<String, Integer> keyNodes = new HashMap<>();

        boolean node = false;

        final XMLStreamReader xmlReader = osmToXML(osm);
        while (wrapXML(xmlReader::hasNext)) {
            switch (wrapXML(xmlReader::next)) {
                case XMLStreamConstants.START_ELEMENT:
                    if (xmlReader.getLocalName().equals("node"))
                        node = true;
                    if (!node || !xmlReader.getLocalName().equals("tag"))
                        continue;
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (xmlReader.getLocalName().equals("node"))
                        node = false;
                default:
                    continue;
            }

            String key = null;
            for (int i = 0; i < xmlReader.getAttributeCount(); i++) {
                if (xmlReader.getAttributeLocalName(i).equals("k")) {
                    key = xmlReader.getAttributeValue(i);
                    break;
                }
            }
            if (key != null)
                keyNodes.put(key, keyNodes.getOrDefault(key, 0) + 1);
        }

        return keyNodes.entrySet().stream()
                .sorted(Comparator.comparingInt(Map.Entry::getValue))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Entry> buildSchema(InputStream osm) throws XMLStreamException {
        final HashSet<Entry> entries = new HashSet<>();

        final ArrayDeque<Entry> parents = new ArrayDeque<>();
        parents.add(Entry.ROOT);

        final XMLStreamReader xmlReader = osmToXML(osm);
        while (wrapXML(xmlReader::hasNext)) {
            switch (wrapXML(xmlReader::next)) {
                case XMLStreamConstants.START_ELEMENT:
                    final String name = xmlReader.getLocalName();

                    final Stream.Builder<String> attributesBuilder = Stream.builder();
                    for (int i = 0; i < xmlReader.getAttributeCount(); i++) {
                        attributesBuilder.accept(xmlReader.getAttributeLocalName(i));
                    }
                    final String[] attributes = attributesBuilder.build().toArray(String[]::new);

                    final Entry entry = new Entry(name, attributes, parents.getLast());
                    parents.addLast(entry);
                    entries.add(entry);
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    parents.removeLast();
            }
        }

        return entries;
    }

    private XMLStreamReader osmToXML(final InputStream osm) throws XMLStreamException {
        return wrapXML(factory::createXMLStreamReader, osm);
    }

    @Override
    public void loadToDB(final InputStream osm) throws XMLStreamException, JAXBException {
        val jc = JAXBContext.newInstance(OsmRoot.class.getPackageName());
        val unmarshaller = jc.createUnmarshaller();

        @Cleanup
        val dbInserter = new DBInserter(jdbc);

        int count = 0;

        val xmlReader = osmToXML(osm);
        while (wrapXML(xmlReader::hasNext)) {
            if (wrapXML(xmlReader::next) != XMLStreamConstants.START_ELEMENT ||
                    !xmlReader.getLocalName().equals("node"))
                continue;

            val node = unmarshaller.unmarshal(xmlReader, OsmNode.class).getValue();
            dbInserter.insertNode(node.getId(),
                    node.getLat(), node.getLon(),
                    node.getVersion(), node.getChangeset(), node.isVisible(),
                    node.getUser(), node.getUid(),
                    new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis()));

            for (val tag : node.getTag()) {
                dbInserter.insertTag(tag.getK(), tag.getV());
                dbInserter.insertNodeTag(node.getId(), tag.getK(), tag.getV());
            }

            if (++count == 10000)
                return;
        }
    }
}
