package ru.nsu.fit.popov.springnsu2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "tag")
@IdClass(Tag.PK.class)
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(of = { "key", "value" })
public class Tag implements Serializable {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PK implements Serializable {
        private String key;
        private String value;
    }

    @Id
    @Column(name = "k", length = 250)
    private String key;

    @Id
    @Column(name = "v", length = 740)
    private String value;

    @ManyToMany(mappedBy = "tags")
    private Set<Node> nodes;

    @ManyToMany(mappedBy = "tags")
    private Set<Way> ways;

    @ManyToMany(mappedBy = "tags")
    private Set<Relation> relations;
}
