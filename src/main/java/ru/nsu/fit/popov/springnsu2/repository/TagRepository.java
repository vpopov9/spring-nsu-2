package ru.nsu.fit.popov.springnsu2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.fit.popov.springnsu2.model.Tag;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Tag.PK> {

    List<Tag> findByKey(String key);

    @Modifying
    @Query(nativeQuery = true, value = "" +
            " DELETE tag, node_tag" +
            " FROM tag" +
            "   JOIN node_tag" +
            "     ON k = tag_k" +
            "       AND v = tag_v" +
            " WHERE k = :k" +
            "   AND v = :v")
    void deleteByKAndV(@Param("k") String k, @Param("v") String v);

    @Override
    default void delete(Tag tag) {
        deleteByKAndV(tag.getKey(), tag.getValue());
    }

    @Query(value = "" +
            " SELECT tag" +
            " FROM Node node" +
            "   JOIN node.tags tag" +
            " WHERE sqrt( (node.lat - :lat) * (node.lat - :lat) +" +
            "             (node.lon - :lon) * (node.lon - :lon) ) < :dist" +
            " ORDER BY sqrt( (node.lat - :lat) * (node.lat - :lat) +" +
            "                (node.lon - :lon) * (node.lon - :lon) ) ASC")
    List<Tag> findNear(@Param("lat") double lat, @Param("lon") double lon,
                       @Param("dist") double dist);
}
