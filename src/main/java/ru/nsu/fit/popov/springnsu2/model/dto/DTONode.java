package ru.nsu.fit.popov.springnsu2.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import lombok.val;
import org.springframework.lang.Nullable;
import ru.nsu.fit.popov.springnsu2.model.Node;
import ru.nsu.fit.popov.springnsu2.model.Tag;

import java.util.Set;
import java.util.stream.Collectors;

@Value
public class DTONode {

    public static DTONode fromNode(final Node node) {
        val tags = node.getTags().stream()
                .map(tag -> new DTOTag(tag.getKey(), tag.getValue()))
                .collect(Collectors.toSet());

        return new DTONode(node.getId(), node.getLat(), node.getLon(), tags);
    }

    public static Node toNode(final DTONode dtoNode, @Nullable Node node) {
        if (node == null)
            node = new Node();

        val tags = dtoNode.getTags().stream()
                .map(dtoTag -> Tag.builder()
                        .key(dtoTag.getKey())
                        .value(dtoTag.getValue())
                        .build())
                .collect(Collectors.toSet());

        node.setId(dtoNode.getId());
        node.setLon(dtoNode.getLon());
        node.setLat(dtoNode.getLat());
        node.setTags(tags);

        return node;
    }

    private long id;

    private double lat;
    private double lon;

    private Set<DTOTag> tags;

    @JsonCreator
    public DTONode(@JsonProperty(value = "id", defaultValue = "0") long id,
                   @JsonProperty(value = "lat", required = true) double lat,
                   @JsonProperty(value = "lon", required = true) double lon,
                   @JsonProperty(value = "tags", defaultValue = "[]") Set<DTOTag> tags) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.tags = tags;
    }
}
