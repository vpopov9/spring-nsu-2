package ru.nsu.fit.popov.springnsu2.rest;

import lombok.experimental.var;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.popov.springnsu2.model.dto.DTONode;
import ru.nsu.fit.popov.springnsu2.repository.NodeRepository;
import ru.nsu.fit.popov.springnsu2.repository.TagRepository;

import java.util.Map;

@RestController
@RequestMapping(path = "/node")
public class NodeController {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private NodeRepository nodeRepository;

    @GetMapping(path = "/{id}")
    public DTONode getById(@PathVariable long id) {
        val node = nodeRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "not found"));

        return DTONode.fromNode(node);
    }

    @GetMapping(path = "/all")
    public DTONode[] getAll() {
        val nodes = nodeRepository.findAll();

        return nodes.stream()
                .map(DTONode::fromNode)
                .toArray(DTONode[]::new);
    }

    @GetMapping(path = "/")
    public DTONode[] getNear(@RequestParam double lat, @RequestParam double lon,
                             @RequestParam double dist) {
        val nodes = nodeRepository.findNear(lat, lon, dist);

        return nodes.stream()
                .map(DTONode::fromNode)
                .toArray(DTONode[]::new);
    }

    @PostMapping(path = "/")
    public ResponseEntity<?> create(@RequestBody DTONode dtoNode) {
        var node = DTONode.toNode(dtoNode, null);

        tagRepository.saveAll(node.getTags());
        node = nodeRepository.save(node);

        return new ResponseEntity<>(Map.of("id", node.getId()), HttpStatus.CREATED);
    }

    @PutMapping(path = "/")
    public void update(@RequestBody DTONode dtoNode) {
        var node = nodeRepository.findById(dtoNode.getId())
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "not found"));
        node = DTONode.toNode(dtoNode, node);

        tagRepository.saveAll(node.getTags());
        nodeRepository.save(node);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable long id) {
        val node = nodeRepository.findById(id)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "not found"));

        nodeRepository.delete(node);
    }
}
