package ru.nsu.fit.popov.springnsu2.osm;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;

@Component
@NoArgsConstructor
@Slf4j
public class OSMProvider {

    @Autowired
    @Qualifier("osm.name")
    private String fileName;

    @Autowired
    @Qualifier("osm.compressor")
    private Class<? extends InputStream> compressorClass;

    public InputStream getOSM(@NonNull final String fileName) throws Exception {
        log.info("using OSM archive file: {}", fileName);

        final InputStream archive;
        try {
            archive = new FileInputStream(fileName);
        } catch (Exception e) {
            log.error("file not found: {}", fileName);
            throw e;
        }

        try {
            return compressorClass.getConstructor(InputStream.class)
                    .newInstance(archive);
        } catch (InstantiationException e) {
            log.error("invalid 'osm.compressor' bean: {}", compressorClass.getName());
            throw e;
        } catch (Exception e) {
            log.error("invalid archive file format: {}", fileName);
            throw e;
        }
    }

    public InputStream getOSM() throws Exception {
        return getOSM(fileName);
    }
}
