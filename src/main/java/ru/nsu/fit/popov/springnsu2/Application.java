package ru.nsu.fit.popov.springnsu2;

import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.cli.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import ru.nsu.fit.popov.springnsu2.osm.OSMParser;
import ru.nsu.fit.popov.springnsu2.osm.OSMProvider;

import java.io.InputStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@SpringBootApplication
@Slf4j
public class Application implements CommandLineRunner {

    private static final String OPT_FILE = "file";
    private static final String OPT_UPDATES = "updates";
    private static final String OPT_KEYS = "keys";
    private static final String OPT_SCHEMA = "schema";
    private static final String OPT_WEB = "web";

    private static final Options OPTIONS = new Options();
    static {
        val fileOption = Option.builder("f")
                .longOpt(OPT_FILE)
                .hasArg()
                .argName("FILE")
                .desc("OSM archive file")
                .build();
        val updatesOption = Option.builder("u")
                .longOpt(OPT_UPDATES)
                .desc("updates done by users")
                .build();
        val keysOption = Option.builder("k")
                .longOpt(OPT_KEYS)
                .desc("unique keys in nodes")
                .build();
        val schemaOption = Option.builder("s")
                .longOpt(OPT_SCHEMA)
                .desc("print XML schema")
                .build();
        val webOption = Option.builder("w")
                .longOpt(OPT_WEB)
                .desc("start web server")
                .build();


        final OptionGroup actionGroup = new OptionGroup()
                .addOption(updatesOption)
                .addOption(keysOption)
                .addOption(schemaOption)
                .addOption(webOption);
        actionGroup.setRequired(true);

        OPTIONS
                .addOption(fileOption)
                .addOptionGroup(actionGroup);
    }

    private static Map<String, Option> options;

    private static void printHelp() {
        final String header = "Work with OSM archive file";
        final String footer = "";

        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("spring-nsu-2", header, OPTIONS, footer, true);
    }

    public static void main(final String[] args) {
        val parser = new DefaultParser();
        final CommandLine cl;
        try {
            cl = parser.parse(OPTIONS, args);
        } catch (ParseException e) {
            log.error("invalid options");
            printHelp();
            return;
        }

        options = Arrays.stream(cl.getOptions())
                .collect(Collectors.toMap(Option::getLongOpt, Function.identity()));
        new SpringApplicationBuilder(Application.class)
                .web(WebApplicationType.NONE)
                .run(args)
                .close();

        if (cl.hasOption(OPT_WEB)) {
            log.info("DB is ready, starting server");
            new SpringApplicationBuilder(Application.class)
                    .web(WebApplicationType.SERVLET)
                    .run();
        }
    }

    @Autowired
    private OSMProvider osmProvider;

    @Autowired
    private OSMParser osmParser;

    @Override
    public void run(final String... args) throws Exception {
        if (args.length == 0)
            return;

        final InputStream tmp;
        if (options.containsKey(OPT_FILE))
            tmp = osmProvider.getOSM(options.get(OPT_FILE).getValue());
        else
            tmp = osmProvider.getOSM();

        @Cleanup
        final InputStream osm = tmp;
        if (options.containsKey(OPT_UPDATES))
            printUpdates(osmParser.countUpdates(osm));
        else if (options.containsKey(OPT_KEYS))
            printNodes(osmParser.countKeys(osm));
        else if (options.containsKey(OPT_SCHEMA))
            printSchema(osmParser.buildSchema(osm));
        else if (options.containsKey(OPT_WEB))
            osmParser.loadToDB(osm);
    }

    private void printUpdates(final List<Map.Entry<String, Integer>> userUpdates) {
        userUpdates.forEach(entry ->
                System.out.printf("%s - %d\n", entry.getKey(), entry.getValue())
        );
    }

    private void printNodes(final List<Map.Entry<String, Integer>> keyNodes) {
        keyNodes.forEach(entry ->
                System.out.printf("%s - %d\n", entry.getKey(), entry.getValue())
        );
    }

    private void printSchema(final Collection<OSMParser.Entry> entries) {
        printWithParent(entries, OSMParser.Entry.ROOT, "");
    }

    private void printWithParent(final Collection<OSMParser.Entry> entries,
                                 final OSMParser.Entry parent, final String space) {
        final String childSpace = "    " + space;
        for (OSMParser.Entry entry : entries) {
            if (!Objects.equals(entry.getParent(), parent))
                continue;

            System.out.printf("%s%s { ", space, entry.getName());
            int i;
            for (i = 0; i < entry.getAttributes().length - 1; i++) {
                System.out.printf("%s, ", entry.getAttributes()[i]);
            }
            System.out.printf("%s }\n", entry.getAttributes()[i]);

            printWithParent(entries, entry, childSpace);
        }
    }
}
