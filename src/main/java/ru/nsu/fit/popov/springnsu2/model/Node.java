package ru.nsu.fit.popov.springnsu2.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "node")
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(of = "id")
public class Node {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "lat", nullable = false)
    private double lat;

    @Column(name = "lon", nullable = false)
    private double lon;

    @Column(name = "version")
    private Integer version;

    @Column(name = "changeset")
    private Long changeset;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "user")
    @Length(min = 1, max = 100)
    private String user;

    @Column(name = "uid")
    private Long uid;

    @Column(name = "_timestamp")
    private Timestamp timestamp;

    @ManyToMany
    @JoinTable(name = "node_tag",
            joinColumns =
                    @JoinColumn(name = "node_id", referencedColumnName = "id", nullable = false),
            inverseJoinColumns = {
                    @JoinColumn(name = "tag_k", referencedColumnName = "k", nullable = false),
                    @JoinColumn(name = "tag_v", referencedColumnName = "v", nullable = false)
            },
            uniqueConstraints =
                    @UniqueConstraint(columnNames = { "node_id", "tag_k", "tag_v" }))
    private Set<Tag> tags;
}
