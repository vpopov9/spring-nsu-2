package ru.nsu.fit.popov.springnsu2.model.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.lang.Nullable;
import ru.nsu.fit.popov.springnsu2.model.Tag;

@Value
public class DTOTag {

    public static DTOTag fromTag(final Tag tag) {
        return new DTOTag(tag.getKey(), tag.getValue());
    }

    public static Tag toTag(final DTOTag dtoTag, @Nullable Tag tag) {
        if (tag == null)
            tag = new Tag();

        tag.setKey(dtoTag.getKey());
        tag.setValue(dtoTag.getValue());

        return tag;
    }

    private String key;
    private String value;

    @JsonCreator
    public DTOTag(@JsonProperty(value = "key", required = true) String key,
                  @JsonProperty(value = "value", required = true) String value) {
        this.key = key;
        this.value = value;
    }
}
