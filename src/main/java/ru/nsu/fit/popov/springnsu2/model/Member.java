package ru.nsu.fit.popov.springnsu2.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "member")
@IdClass(Member.PK.class)
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@EqualsAndHashCode(of = { "type", "ref" })
public class Member {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PK implements Serializable {
        private String type;
        private long ref;
    }

    @Id
    @Column(name = "type", length = 8)
    private String type;

    @Id
    @Column(name = "ref")
    private long ref;

    @Column(name = "role")
    @Length(min = 1, max = 250)
    private String role;
}
