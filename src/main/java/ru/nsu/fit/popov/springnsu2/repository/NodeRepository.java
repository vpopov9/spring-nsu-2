package ru.nsu.fit.popov.springnsu2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.nsu.fit.popov.springnsu2.model.Node;

import java.util.List;

public interface NodeRepository extends JpaRepository<Node, Long> {

    @Override
    @Modifying
    @Query(nativeQuery = true, value = "" +
            " DELETE node, node_tag" +
            " FROM node" +
            "   JOIN node_tag" +
            "     ON id = node_id" +
            " WHERE id = :id")
    void deleteById(@Param("id") Long id);

    @Override
    default void delete(Node node) {
        deleteById(node.getId());
    }

    @Query(value = "" +
            " SELECT node" +
            " FROM Node node" +
            " WHERE sqrt( (node.lat - :lat) * (node.lat - :lat) +" +
            "             (node.lon - :lon) * (node.lon - :lon) ) < :dist" +
            " ORDER BY sqrt( (node.lat - :lat) * (node.lat - :lat) +" +
            "                (node.lon - :lon) * (node.lon - :lon) ) ASC")
    List<Node> findNear(@Param("lat") double lat, @Param("lon") double lon,
                        @Param("dist") double dist);
}
