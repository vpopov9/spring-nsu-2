package ru.nsu.fit.popov.springnsu2.osm;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.Closeable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DBInserter implements Closeable {

    enum Type {
        NODE,
        TAG,
        NODE_TAG
    }

    private static final Map<Type, String> SQLS = Map.of(
            Type.NODE, "" +
                    " INSERT INTO node(id, lat, lon," +
                    "                  version, changeset, visible, user, uid, _timestamp)" +
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)" +
                    " ON DUPLICATE KEY UPDATE id=id",
            Type.TAG, "" +
                    " INSERT INTO tag(k, v)" +
                    " VALUES(?, ?)" +
                    " ON DUPLICATE KEY UPDATE k=k",
            Type.NODE_TAG, "" +
                    " INSERT INTO node_tag(node_id, tag_k, tag_v)" +
                    " VALUES(?, ?, ?)" +
                    " ON DUPLICATE KEY UPDATE node_id=node_id"
    );

    private static final int MAX_COUNT = 50000;

    private final JdbcTemplate jdbc;

    private final Map<Type, ArrayList<Object[]>> args = new HashMap<>();
    
    private int count = 0;
    private boolean ready = false;
    private final Object lock = new Object();

    private final Thread worker;

    public DBInserter(final JdbcTemplate jdbc) {
        this.jdbc = jdbc;

        for (val type : SQLS.keySet()) {
            args.put(type, new ArrayList<>());
        }
        
        worker = new Thread(() -> {
            val _args = new HashMap<Type, ArrayList<Object[]>>();
            while (!Thread.interrupted()) {
                synchronized (lock) {
                    ready = true;
                    lock.notify();

                    try {
                        if (count < MAX_COUNT) {
                            lock.wait();
                        }
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    count = 0;

                    ready = false;
                    _args.putAll(args);
                }

                for (val entry : SQLS.entrySet()) {
                    val type = entry.getKey();
                    val sql = entry.getValue();

                    jdbc.batchUpdate(sql, _args.get(type));
                }
            }
        });
        worker.start();
    }

    private void update() {
        try {
            synchronized (lock) {
                if (++count >= MAX_COUNT) {
                    if (!ready)
                        lock.wait();
                    lock.notify();
                }
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            synchronized (lock) {
                count = MAX_COUNT;
                if (!ready)
                    lock.wait();
            }

            worker.interrupt();
            worker.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    void insertNode(final long id,
                    final double lat, final double lon,
                    final Integer version, final Long changeset, final Boolean visible,
                    final String user, final Long uid, final Timestamp timestamp) {
        args.get(Type.NODE).add(new Object[]{
                id, lat, lon, version, changeset, visible, user, uid, timestamp
        });
        
        update();
    }
    
    void insertTag(final String k, final String v) {
        args.get(Type.TAG).add(new Object[]{
                k, v
        });

        update();
    }

    void insertNodeTag(final long nodeId, final String tagK, final String tagV) {
        args.get(Type.NODE_TAG).add(new Object[]{
                nodeId, tagK, tagV
        });

        update();
    }
}
