package ru.nsu.fit.popov.springnsu2.osm;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;

@Configuration
public class OSMConfiguration {

    @Bean(value = "osm.name")
    public String getOSMName() {
        return "RU-NVS.osm.bz2";
    }

    @Bean(value = "osm.compressor")
    public Class<? extends InputStream> getOSMCompressorClass() {
        return BZip2CompressorInputStream.class;
    }
}
