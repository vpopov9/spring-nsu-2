package ru.nsu.fit.popov.springnsu2.rest;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.popov.springnsu2.model.Tag;
import ru.nsu.fit.popov.springnsu2.model.dto.DTOTag;
import ru.nsu.fit.popov.springnsu2.repository.TagRepository;

@RestController
@RequestMapping(path = "/tag")
public class TagController {

    @Autowired
    private TagRepository tagRepository;

    @GetMapping(path = "/{key}")
    public DTOTag[] getByKey(@PathVariable String key) {
        val tags = tagRepository.findByKey(key);

        return tags.stream()
                .map(DTOTag::fromTag)
                .toArray(DTOTag[]::new);
    }

    @GetMapping(path = "/")
    public DTOTag[] getNear(@RequestParam double lat, @RequestParam double lon,
                            @RequestParam double dist) {
        val tags = tagRepository.findNear(lat, lon, dist);

        return tags.stream()
                .map(DTOTag::fromTag)
                .toArray(DTOTag[]::new);
    }

    @DeleteMapping(path = "/{key}/{value}")
    public void delete(@PathVariable String key, @PathVariable String value) {
        val tag = tagRepository.findById(new Tag.PK(key, value))
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "not found"));

        tagRepository.delete(tag);
    }
}
